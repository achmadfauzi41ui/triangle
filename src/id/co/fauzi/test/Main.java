package id.co.fauzi.test;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter your height of triangle: ");
        int rows = in.nextInt();
        for(int k = 0;k<rows;k++){
            for (int l=rows-k; l>0; l--){
                System.out.print("   ");
            }
            for (int l=k+1; l>0; l--){
                System.out.print(" # ");
            }
            System.out.println("\n");
        }

    }
}
